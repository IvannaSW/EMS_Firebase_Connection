import template from './firedb.html';
import controller from './firedb.controller';
import './firedb.scss';

let firedbComponent = {
  template,
  controller
};

export default firedbComponent;
